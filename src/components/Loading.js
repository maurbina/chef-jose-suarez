import React from "react";
import PropTypes from "prop-types";
//npm install react-transition-group --save
import { CSSTransition } from "react-transition-group";
//npm install react-spinners --save
import { BounceLoader } from "react-spinners";

export const Loading = props => {
  return (
    <CSSTransition
      in={props.isLoading}
      timeout={500}
      classNames="fade-in"
      unmountOnExit
    >
      <div className="App-overlay">
        <div style={{ width: "155px" }} className="App-center-loading">
          <h4 className="text-center">{props.loadingMessage}</h4>
          <BounceLoader size={150} color={"#537DBF"} loading={props.loading} />
        </div>
      </div>
    </CSSTransition>
  );
};

Loading.propTypes = {
  isLoading: PropTypes.bool,
  loadingMessage: PropTypes.string
};

export default Loading;