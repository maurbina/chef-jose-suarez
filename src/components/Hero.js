
//Dependencies
import React, { Component } from "react";
import "./hero.css";

class Content extends Component {
  render() {
    return (
        <div className="heroChef heroGrego">
        <img className="firstimg" src={this.props.fondo} alt="chef cocinero bogota jose gregorio suarez" title="chef cocinero bogota jose gregorio suarez" />
        <img className="hideimg" src={this.props.fruta} alt="chef cocinero bogota jose gregorio suarez" title="chef cocinero bogota jose gregorio suarez" />
      </div>
    );
  }
}

export default Content;


