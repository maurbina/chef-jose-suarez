//Dependencies
import React from "react";
import { Switch, Route } from "react-router-dom";

//Componentes
import App from "./modules/App";
import Home from "./modules/home";
import Profesional from "./modules/profesional";
import Recetas from "./modules/recetas";

const AppRouter = () => (
  <App>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/profesional" component={Profesional} />
      <Route exact path="/recetas" component={Recetas} />
    </Switch>
  </App>
);

export default AppRouter;