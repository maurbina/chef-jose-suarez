//Dependencies
import React, { Component } from "react";
import './logos.css';
import logo1 from '../../../assets/altamiralogo.jpg';
import logo2 from '../../../assets/mokambologo.png';
import logo3 from '../../../assets/italo logo.jpg';
import logo4 from '../../../assets/atflogo.jpg';
import logo5 from '../../../assets/strettologo.png';

class Logos extends Component {
  render() {
    return (
      <div>
        <h1 className="titulo">Restaurantes</h1>
      <div className="logos">
        <div><img src={logo1} alt='Hotel Altamira Suites' title='Hotel Altamira Suites' /></div>
        <div><img src={logo2} alt='Mokambo Caffe' title='Mokambo Caffe'/></div>
        <div><img src={logo5} alt='Stretto Cafe' title='Stretto Cafe' /></div>
        <div><img src={logo3} alt='Centro Italo Venezolano' title='Centro Italo Venezolano' /></div>
        <div><img src={logo4} alt='Azimo Bogota' title='Azimo Bogota'  /></div>
      </div>
      </div>
    );
  }
}

export default Logos;
