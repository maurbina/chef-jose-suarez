//Dependencies
import React, { Component } from "react";
import "./Home.css";
import face from "../../assets/chef2.jpeg";
import Logos from "../home/logos";
import Hero from "../../components/Hero";
import Recetas from "./recetas";
import hero from "../../assets/ensalada.jpg";
import fruta from "../../assets/ensaladafrutas.jpg";

class Home extends Component {
  render() {
    return (
      <div>
        <Hero fondo={hero} fruta={fruta} />
        <div className="about">
          <div className="aboutImg">
            <img src={face} alt="chef cocinero bogota jose gregorio suarez" title="chef cocinero bogota jose gregorio suarez" />
          </div>
          <div className="aboutContent" >
            <h1 id="acerca">Acerca</h1>
            <p>
              José Gregorio Suárez Suárez chef profesional. Llevado por
              la pasión de la gastronomía decidió adquirir una carrera culinaria
              en la academia Aerotécnica Floriven II. Donde obtuvo el
              certificado de Chef Internacional y Gerente de Cocina, ya contaba
              con 10 años de experiencia en diferentes restaurantes y locales de comida,
              desempeñándose como cocinero principal.
            </p>
            <p>
              En sus platos se refleja todos los años de experiencia, su
              espontaneidad en la cocina y su carisma, plasman una marca de
              calidad en sus platos. En sus dos últimos años lidero dos grandes
              proyectos, el restaurante Mokambo Caffee con su deliciosa
              gastronomía Italiana cocinando el famoso risotto, y estuvo al
              frente como Chef de cocina en el reconocido Hotel Altamira Suites. En 2019 tuvo la oportunidad 
              de trabajar el restaurant Azimo Bogotá donde nuevamente demuestra el talento culinario en sus platillos.
            </p>
            <p>
              {" "}
              <strong>Colombia, Bogotá 2019</strong>
            </p>
          </div>
        </div>
        <Logos />
        <Recetas />
      </div>
    );
  }
}

export default Home;
