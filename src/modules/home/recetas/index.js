//Dependencies
import React, { Component } from "react";
import entraCaliente from '../../../assets/marmitako.jpg';
import entraCruda from '../../../assets/rollodetrucha.jpg';
import ensaladas from '../../../assets/paella.jpg';
import pastas from '../../../assets/triopasta.jpg';
import risottos from '../../../assets/risottonew.jpg';
import hambur from '../../../assets/hamburguer.jpg';
import desayunos from '../../../assets/desayuno.jpg';
import raciones from '../../../assets/empanadasnew.jpg';
import tapas from '../../../assets/croquetas.jpg';
import './Recetas.css';

class Recetas extends Component {
  render() {
    return (
      <div>
        <h1 className="titulo">Sus platos</h1>
      <div className="recetas">
        <div><img src={entraCaliente} alt='Marmitako de Salmon' title='Marmitako de Salmon'  /><h1>Marmitako de Salmon</h1></div>
        <div><img src={entraCruda} alt='Rollo de Trucha'  title='Rollo de Trucha' /><h1>Rollo de Trucha</h1></div>
        <div><img src={ensaladas} alt='paella' title='palle' /><h1>Paella</h1></div>
        <div><img src={pastas} alt='pastas' title='pastas'  /><h1>Trio de Pasta</h1></div>
        <div><img src={risottos} alt='risottos' title='risottos'  /><h1>Risotto Camarones</h1></div>
        <div><img src={hambur} alt='hamburguesas' title='hamburguesas'  /><h1>Cheese Burger</h1></div>
        <div><img src={desayunos} alt='desayunos' title='desayunos'  /><h1>Desayuno Criollo</h1></div>
        <div><img src={raciones} alt='risottos' title='risottos'  /><h1>Empanadas de Asado Negro</h1></div>
        <div><img src={tapas} alt='tapas' title='tapas'  /><h1>Croquetas de Quezo</h1></div>
      </div>
      </div>
    );
  }
}

export default Recetas;
