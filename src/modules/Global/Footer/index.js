//Dependencies
import React, { Component } from "react";
import "./Footer.css";

class Footer extends Component {
  render() {
    return (
      <div className="Footer">
        <h1>Chef Colombia</h1>
        <div>
          <a
            href="https://www.instagram.com/gregomau/"
            class="fa fa-instagram"
            rel="noopener noreferrer"
            target="_blank"
          />
        </div>
        <h1>350 792 0771</h1>
        <h5>@Copyright 2019 Colombia, Bogotá</h5>
      </div>
    );
  }
}

export default Footer;
