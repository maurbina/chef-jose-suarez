import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Header.css";
import imag from "../../../assets/nombre.png";

class Header extends Component {
  render() {
    const { items } = this.props;
    return (
      <div className="Header">
        <div className="imgNombre">
           <a href="/"> 
            <img src={imag} alt="chef cocinero  bogota jose gregorio suarez" title="chef cocinero bogota jose gregorio suarez" />
          </a>
        </div>
        <div className="containerMenu">
          <ul className="Menu">
            {items &&
              items.map((item, key) => (
                <li key={key}>
                  {" "}
                  <Link to={item.url}> {item.title} </Link>
                </li>
              ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Header;
