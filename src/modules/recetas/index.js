import React, { Component } from "react";
import "./Recetas.css";
//Data
import recetas from "../../data/recetas";

class Recetas extends Component {
  render() {
    return (
      <div>
        <div className="recetasUn">
          {recetas &&
            recetas.map((receta, i) => (
              <div key={i + "num"}>
                <h2 className="tituloRec"> {receta.title}</h2>
                {receta.platos.map((p, i) => (
                  <p className="contenido">{p}</p>
                ))}
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default Recetas;
