//Dependencies
import React, { Component } from "react";

//Components
import Header from "./Global/Header";
import Content from "./Global/Content";
import Footer from "./Global/Footer";
import Loading from "../components/Loading";

//Data
import items from "../data/menu";

// Assets
import "./App.css";

class App extends Component {
  state = {
    isLoading: true,
    loadingI18n: "Cargando"
  };
  componentDidMount = () => {
    this.setState({
      isLoading: false
    });
  };

  render() {
    let content = null;
    const { children } = this.props;
      this.state.isLoading
        ? (content = (
            <Loading
              isLoading={this.state.isLoading}
              loadingMessage={this.state.loadingI18n}
            />
          ))
        : (content = (
            <div className="App">
              <Header items={items} /> <Content body={children} /> <Footer />
            </div>
          ));
      return <div>{content}</div>;
    
  }
}

export default App;
