//Dependencies
import React, { Component } from "react";
import "./Profesional.css";

//Data
import empleos from "../../data/empleos";

class Profesional extends Component {
  render() {
    return (
        <div className="altamiraSuite">
          {empleos &&
            empleos.map( (empleo, i )  => (
              <div key={i+'num'}>
                <hr></hr>
                <h1> {empleo.title}</h1>
                <i class="fa fa-cog fa-spin"></i>
                <h2>{empleo.cargo}</h2>
                <p>
                  {empleo.funcion}
                </p>
                <h3 className="fechas">{empleo.fecha}</h3>
                <hr></hr>
              </div>
            ))}
        </div>
    );
  }
}

export default Profesional;
