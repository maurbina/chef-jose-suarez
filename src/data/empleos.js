export default [
  {
    title: "Azimo Restaurant Bogota",
    cargo: "Cocinero",
    funcion:
      "Apoyar directamente al Chef en la cocina",
    fecha: "2019"
  },
  {
    title: "Altamira Suites Restautant",
    cargo: "Chef de Cocina",
    funcion:
      "Apoyar directamente al Chef , realizar horarios de los cocineros, apoyar con las sugerencias al chef, manejar personal a cargo del cocinero de cada estación, solicitar pedidos de legumbres, víveres y productos primordiales. Supervisar Mice Place de los cocineros, entrada y salida además de planificar compras e inventarios de cocina",
    fecha: "2018 hasta junio del 2019"
  },
  {
    title: "Mokambo Cafe Restaurant",
    cargo: "Sous Chef",
    funcion:
      "Apoyar directamente al Chef , realizar horarios de los cocineros, apoyar con las sugerencias al chef, manejar personal a cargo del cocinero de cada estación, solicitar pedidos de legumbres, víveres y productos primordiales. Supervisar Mice Place de los cocineros, entrada y salida además de planificar compras e inventarios de cocina",
    fecha: "2016 hasta diciembre del 2018"
  },
  {
    title: "Cocina Mi Tita",
    cargo: "Cocinero",
    funcion:
      "Cocinar menú diario bajo los estándares establecidos por el instituto para alumnos, profesores y demás personal. Atender al proveedor y recibir los productos y alimentos. Verificar el inventario para evitar falta en la dispensa, y no afecte la cocción completa de los menús diarios",
    fecha: "2014 hasta enero del 2016"
  },
  {
    title: "Stretto Café Restaurant",
    cargo: "Jefe de Cocina",
    funcion:
      "Preparar platillos al personal del local, coordinar la cocina, sacar las comandas, mantener área de trabajo limpia y organizada. Atender al proveedor, recibir los productos y alimentos. Verificar el inventario para evitar falta de alimentos en la dispensa.",
    fecha: "2012 hasta marzo del 2014"
  }
];
